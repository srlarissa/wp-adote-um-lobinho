<?php
// Template Name: Busca de lobinhos cadastrados
?>


<?php get_header(); ?>

        <main>
            <div class="barra-busca">
                <section class = "busca">
                    <?php get_search_form() ?>
                </section>
                <span class ="marcador">
                    <input type = "checkbox" id="adopted" name="adopted">
                    <label class = "adotados" for="adopted">Ver lobinhos adotados</label>
                </span>
           
            </div>
    
            <div class="wolf-list">
                        
            </div>
             
        </main>
        <div class="gradiente">
        </div>

        <?php echo paginate_links() ?>

<?php get_footer(); ?>

