<?php
// Template Name: Adicionar novo lobinho
?>

<?php get_header(); ?>

        <main>
            
        <section class = "painel">
            <section class = "container">
                <h3>Coloque um Lobinho para Adoção</h3>
                    <section id="container-text">
                       
                            <p>Nome do Lobinho:</p>
                            <textarea class ="messagem" id="name" rows="1" cols = "50" required="required" minlength="4" maxlength="60" placeholder="Digite o nome ..."></textarea>
                            <p>Anos:</p>
                            <textarea class ="messagem" id="age" rows="1" cols = "20" required="required" minlength="0" maxlength="100" placeholder="Ano..."></textarea>
                            <p>Link da Foto:</p>
                            <textarea class ="messagem" id="photo" rows="1"  cols = "80" required="required" placeholder="Digite o link..."></textarea>
                            <p>Descrição:</p>
                            <textarea class ="messagem" id="descrip" rows="10" cols = "80" required="required" minlength="10" maxlength="255"placeholder="Digite a descrição..."></textarea>
                            <button class="botao2">Salvar</button>
                     
                        
                    </section> 
            </section>
        </section>
            
        </main>
        <div class="gradiente">
        </div>

<?php get_footer(); ?>
