<?php 

    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'start_post_rel_link', 10, 0 );
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    remove_action('wp_head', 'feed_links_extra', 3);
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');

    function my_theme_style_function(){
        wp_enqueue_style('reset.css', get_template_directory_uri() . 'reset.css');
        wp_enqueue_style('style.css', get_template_directory_uri() . 'style.css');
    }

    add_action('wp_enqueue_scripts', 'my_theme_style_function');

    // function my_theme_scripts_function(){
    //     wp_enqueue_script('myscript', get_template_directory_uri() . '/script.js');
    // }

    // add_action('wp_enqueue_script', 'my_theme_scripts_function');


?>