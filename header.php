<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php bloginfo('Adote um lobinho') ?> </title><!--Permite que o titulo da página seja trocado utilizando o wp-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <!--Fonte dos links do topo-->

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Darker+Grotesque:wght@700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
     <?php wp_head(); ?> <!--exporta o header para ser utilizado no index -->
</head>
<body>
    <section>
        <header class="menu-topo">
            <p class="links-topo"><a href="http://adote-seu-lobinho.local/busca/">Nossos Lobinhos</a></p>
            <a href="http://adote-seu-lobinho.local/sample-page/"><img class="image" src="<?php echo get_stylesheet_directory_uri() ?>/Adote um Lobinho 2021.3 (Copy)/slogan.png" alt="slogan"  id="teste"></a>
            <div>
                <p ><a class="links-topo2 nodecoration" href="http://adote-seu-lobinho.local/quem-somos/">Quem Somos</a></p>
                <h2><a class = "seta nodecoration" href="http://adote-seu-lobinho.local/quem-somos/">Somos lob{IN}hos!</a></h2>
            </div>

        </header>