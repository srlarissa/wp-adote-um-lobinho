//add_lobinho -----------------------------------------------------------------------------------------------------------

const url = "https://lobinhos.herokuapp.com" //Link da API

const postWolf = () => {

    const nameScreen = document.querySelector('#name').value; //Retornará os valores colocados nos inputs da tela
    const ageScreen = document.querySelector('#age').value;
    const photoScreen = document.querySelector('#photo').value;
    const descriptionScreen = document.querySelector('#descrip').value;
    const bodyAdd = { //criará o body do objeto que será enviado para a API
        wolf:{
    
            name: nameScreen,
            age: ageScreen,
            image_url: photoScreen,
            description: descriptionScreen
            
        }
   }

   const config = { //Configurará o método post -- PADRÃO
       method:"POST",
       body: JSON.stringify(bodyAdd), //Fará o parse do json para string para que seja adicionado ao sistema
       headers:{
           "Content-Type":"application/json"
       }
    }
    fetch(url + "/wolves" , config) //fará o método POST funcionar
    .then((response) => response.json())
    .then((w) => {
        alert(`O novo Lobinho ${w.name} foi adicionado com sucesso!`)  
    })
    .catch(error => {
       console.error(error)
    })
}

const btnSave = document.querySelector('.botao2')
btnSave.addEventListener("click", postWolf)


//adotar---------------------------------------------------------------------------------------------------------------

const wolfAdopt = (wolf) => {

    //Retorno das informações do adotante
    const adopterName = document.querySelector('.inputmaior')
    const adopterMail = document.querySelector('.inputmaior2')
    const adopterAge = document.querySelector('.inputmenor')

    const bodyAdopt = {
        wolf:{
            adopter_name: adopterName.value,
            adopter_email: adopterMail.value,
            adopter_age: adopterAge.value,
            adopted: true
        }
    }
      
    fetch(url + "/wolves/" + wolf.id, {
        method: "PUT",
        body: JSON.stringify(bodyAdopt),
        headers: {
            "Content-Type":"application/json"
        }})
    .then(response => response.json())
    .then(wolf => {
        alert(`${wolf.name} foi adotado com sucesso!`)
    })
    .catch(error => {
        console.error(error)
    })
            
}
const getThatWolfAdopt = () => {
    const urlParamAdopt = new URLSearchParams(window.location.search).get('id')
    fetch(url + "/wolves/" + urlParamAdopt)
    .then(response => response.json())
    .then(wolf => {
       wolfAdopt(wolf)
    })
    .catch(error => {
        console.error(error)
    })
}

const btnAdocao = document.querySelector('.adoption')
btnAdocao.addEventListener("click", getThatWolfAdopt)

//busca-lobinhos -------------------------------------------------------------------------------------------------------

const screen = document.querySelector('.wolf-list')//Retornará a ul responsável pela renderização dos resultados na tela

//Função que criará a div que retornará o lobo
const creatediv = (wolf) => {
    const li = document.createElement('li')

    li.innerHTML = `
        <div class = "texto-imagem">
            <img class = "lobinho" src = "${wolf.image_url}">
            <div class = "descricao_lobinho">
                <p>${wolf.name}</p>
                <p>Idade: ${wolf.age} anos</p>
                <p>${wolf.description}</p>
            </div>
        </div>
    `

    if (wolf.adopted == false){
        li.innerHTML += `
            <a href="Layout-Show_Lobinho.html?id=${wolf.id}" target="blank"><button class="botao4" value="${wolf.id}">Adotar</button></a>
        `
    }

    if(wolf.adopted == true){
        li.innerHTML += `
            <button class="botao4" value="${wolf.id}">Adotado</button></a>
        `
    }
    screen.appendChild(li)
    
}

//Função que recuperará os dados da API
const getWolves = () => {
    const txtSearch = document.querySelector('#search').value //Captação do input de busca
   
    while (screen.firstChild){
        screen.removeChild(screen.firstChild)
    } //Limpar a tela antes de cada busca

    fetch(url + "/wolves") //A resposta será um fetch do url da API juntamente, retornando a promise resolvida
    .then((response) => response.json()) // Transfomará a promise resolvida em uma prommise contendo o objeto .json
    .then((data) => { //filtra os objetos e retorna de acordo com o texto indicado no input
        const datafiltra = data.filter(wolf => !txtSearch || wolf.name.includes(txtSearch))
        datafiltra.forEach (wolf => { 
            creatediv(wolf)
        })           
    })
    .catch((error) => { //Caso de algum erro, este será retornado como uma mensagem de erro no console
        console.error(error);
    })
}

const getAdoptedWolves = () => {
    const txtSearch = document.querySelector('#search').value //Captação do input de busca
    const screen = document.querySelector('.wolf-list')//Retornará a ul responsável pela renderização dos resultados na tela

    while (screen.firstChild){
        screen.removeChild(screen.firstChild)
    } //Limpar a tela antes de cada busca

    fetch(url + "/wolves/adopted") //A resposta será um fetch do url da API juntamente, retornando a promise resolvida
    .then((response) => response.json()) // Transfomará a promise resolvida em uma prommise contendo o objeto .json
    .then((data) => { //filtra os objetos e retorna de acordo com o texto indicado no input
        const datafiltra = data.filter(wolf => !txtSearch || wolf.name.includes(txtSearch))
        datafiltra.forEach (wolf => { 
            creatediv(wolf)
        })           
    })
    .catch((error) => { //Caso de algum erro, este será retornado como uma mensagem de erro no console
        console.error(error);
    })
}


const btnSearch = document.querySelector('#btn-search');
const adopted = document.querySelector('#adopted')
btnSearch.addEventListener("click", () =>{
    if(!adopted.checked){
        getWolves()
    }else{
        getAdoptedWolves()
    }
});

getWolves()

//index -----------------------------------------------------------------------------------------------------------------

const screenIndex = document.querySelector('.wolf-list')//Retornará a ul responsável pela renderização dos resultados na tela
const createdivIndex = (wolf) => {
    const liIndex = document.createElement('li')

    liIndex.innerHTML = `
        <div class = "texto-imagem">
            <a href="Layout-Show_Lobinho.html?id=${wolf.id}" target="blank"><img class = "lobinho" src = "${wolf.image_url}" align="left"></a>
            <div class = "descricao_lobinho">
                <h1>${wolf.name}</h1>
                <p>Idade: ${wolf.age} anos</p>
                <p>${wolf.description}</p>
            </div>
        </div>
        
    `
    screenIndex.appendChild(li)
    
}

const getWolvesIndex = () => {
  
    while (screenIndex.firstChild){
        screenIndex.removeChild(screenIndex.firstChild)
    } //Limpar a tela antes de cada busca

    fetch(url + "/wolves") //A resposta será um fetch do url da API juntamente, retornando a promise resolvida
    .then((response) => response.json()) // Transfomará a promise resolvida em uma prommise contendo o objeto .json
    .then((data) => { //filtra os objetos e retorna de acordo com o texto indicado no input
       creatediv(data[0])     
       creatediv(data[1])             
    })
    .catch((error) => { //Caso de algum erro, este será retornado como uma mensagem de erro no console
        console.error(error);
    })
}

getWolvesIndex()

//show -----------------------------------------------------------------------------------------------------------------

const urlParam = new URLSearchParams(window.location.search).get('id')

const screenDiv = document.querySelector('.adopt')

const wolfMaker = (wolf) => {
    const photo = wolf.image_url
    const descrip = wolf.description
    const name = wolf.name
    const id = wolf.id
    const wolfDiv = document.createElement('div')
    wolfDiv.innerHTML = `
    <div class="title">
        <h1>${name}</h1>
    </div>
    <div class="main flex">
        <div class="content1 flex">
            <div class="imagem">
                <img src="${photo}" alt="Foto do lobinho ${name}">             
            </div>
            <div class="button flex">
                <a href="Layout-Adotar_lobinho.html?id=${id}" target="blank"><button class="button1">adotar</button></a>
                <button class="btn-delete" onclick="deleteThatWolf()">excluir</button>
            </div>
        </div>
        <div class="content2 flex">
            <p class="texto1">${descrip}</p>
        </div>    
    </div>
    `
   screenDiv.appendChild(wolfDiv)
}


const getThatWolfShow = () => {
    fetch(url + "/wolves/" + urlParam)
    .then(response => response.json())
    .then(wolf => {
        wolfMaker(wolf)
    })
    .catch(error => {
        console.error(error)
    })
}
getThatWolfShow()


const deleteThatWolf = () => {
    fetch(url + "/wolves/" + urlParam, {method: "DELETE"})
    .then(() =>{
        alert('Lobinho apagado com sucesso!')
    })
    .catch(error => {
        console.error(error)
    })
    
}

